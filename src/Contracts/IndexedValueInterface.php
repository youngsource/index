<?php

namespace Laudis\Index\Contracts;

/**
 * @template T
 */
interface IndexedValueInterface
{
    /**
     * @return T
     */
    public function getValue();

    /**
     * @return T
     */
    public function getOriginalValue();

    /**
     * @return IndexTypeInterface
     */
    public function getType(): IndexTypeInterface;
}
