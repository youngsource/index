<?php

namespace Laudis\Index\Contracts;

use DateTimeInterface;
use Laudis\Index\Exceptions\IndexedValueNotFoundException;

interface ContextualIndexedValueRepositoryInterface
{
    /**
     * Gets the value with a given context.
     * @param DateTimeInterface $dateTime
     * @param string $identifier
     * @return IndexedValueInterface
     *
     * @throws IndexedValueNotFoundException
     */
    public function getFromDate(DateTimeInterface $dateTime, string $identifier): IndexedValueInterface;
}
