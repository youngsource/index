<?php

namespace Laudis\Index\Contracts;

interface IndexedValueRepositoryInterface
{
    /**
     * @param string $name
     * @return IndexedValueInterface
     */
    public function get(string $name): IndexedValueInterface;
}
