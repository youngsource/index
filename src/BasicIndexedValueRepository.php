<?php

namespace Laudis\Index;

use function is_callable;
use Laudis\Index\Contracts\IndexedValueInterface;
use Laudis\Index\Contracts\IndexedValueRepositoryInterface;

final class BasicIndexedValueRepository implements IndexedValueRepositoryInterface
{
    /** @var IndexedValueInterface[]|callable():IndexedValueInterface[] */
    private $indexedValues = [];

    public function register(string $name, $value): void
    {
        $this->indexedValues[$name] = $value;
    }

    /**
     * @param string $name
     * @return IndexedValueInterface
     */
    public function get(string $name): IndexedValueInterface
    {
        $value = $this->indexedValues[$name];
        assert($value instanceof IndexedValueInterface || is_callable($value));
        if ($value instanceof IndexedValueInterface) {
            return $value;
        }
        return $value();
    }
}
