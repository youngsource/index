<?php

namespace Laudis\Index;

use Laudis\Index\Contracts\IndexTypeInterface;

/**
 * Class IndexType
 *
 * @method static IndexTypeInterface EURO()
 * @method static IndexTypeInterface PERCENTAGE()
 * @method static IndexTypeInterface CONSTANT()
 *
 * @method static IndexTypeInterface resolve($constValue)
 * @method string getValue()
 */
final class IndexType extends TypedEnum implements IndexTypeInterface
{
    private const EURO = 'euro';
    private const PERCENTAGE = 'percentage';
    private const CONSTANT = 'constant';
}
