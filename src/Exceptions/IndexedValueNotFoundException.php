<?php

namespace Laudis\Index\Exceptions;

use RuntimeException;

class IndexedValueNotFoundException extends RuntimeException
{

}
