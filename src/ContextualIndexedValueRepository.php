<?php

namespace Laudis\Index;

use DateTimeInterface;
use Laudis\Index\Contracts\ContextualIndexedValueRepositoryInterface;
use Laudis\Index\Contracts\IndexedValueInterface;
use Laudis\Index\Exceptions\IndexedValueNotFoundException;

final class ContextualIndexedValueRepository implements ContextualIndexedValueRepositoryInterface
{
    private $mappings = [];

    public function addIndex(string $identifier, DateTimeInterface $dateTime, IndexedValueInterface $scale): void
    {
        if (!isset($this->mappings[$identifier])) {
            $this->mappings[$identifier] = [];
        }
        $this->mappings[$identifier][$dateTime->getTimestamp()] = $scale;
    }

    /**
     * Gets the value with a given context.
     * @param DateTimeInterface $dateTime
     * @param string $identifier
     * @return IndexedValueInterface
     */
    public function getFromDate(DateTimeInterface $dateTime, string $identifier): IndexedValueInterface
    {
        $current = $this->findScale($dateTime, $identifier);
        if ($current === null) {
            throw new IndexedValueNotFoundException('Cannot find scale item: ' . $identifier);
        }
        return $current;
    }

    /**
     * @param DateTimeInterface $dateTime
     * @param string $identifier
     * @return IndexedValueInterface|null
     */
    private function findScale(DateTimeInterface $dateTime, string $identifier): ?IndexedValueInterface
    {
        if (!isset($this->mappings[$identifier])) {
            return null;
        }

        $set = $this->mappings[$identifier];
        ksort($set);
        $time = $dateTime->getTimestamp();
        $current = null;
        foreach ($set as $key => $value) {
            if ($time < $key) {
                break;
            }
            $current = $value;
        }

        return $current;
    }
}
