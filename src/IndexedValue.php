<?php

namespace Laudis\Index;

use Laudis\Index\Contracts\IndexedValueInterface;
use Laudis\Index\Contracts\IndexTypeInterface;

/**
 * Class IndexedValue
 *
 * @template T
 */
final class IndexedValue implements IndexedValueInterface
{
    /** @var T */
    private $value;
    /** @var IndexType */
    private $type;
    /** @var float|int */
    private $originalValue;

    /**
     * IndexedValue constructor.
     * @param int|float $value
     * @param int|float $originalValue
     * @param IndexTypeInterface $type
     */
    public function __construct($originalValue, $value, IndexTypeInterface $type)
    {
        $this->value = $value;
        $this->type = $type;
        $this->originalValue = $originalValue;
    }

    /**
     * @param int|float $originalValue
     * @param int|float $value
     * @param IndexTypeInterface $type
     * @return IndexedValue
     */
    public static function make($originalValue, $value, IndexTypeInterface $type): IndexedValue
    {
        return new self($originalValue, $value, $type);
    }

    /**
     * @return int|float
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return IndexTypeInterface
     */
    public function getType(): IndexTypeInterface
    {
        return $this->type;
    }

    /**
     * @return int|float
     */
    public function getOriginalValue()
    {
        return $this->originalValue;
    }
}
