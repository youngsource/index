<?php

namespace Laudis\Index\Testing;

use Laudis\Index\BasicIndexedValueRepository;
use Laudis\Index\IndexedValue;
use Laudis\Index\IndexType;
use Laudis\Index\Contracts\IndexedValueInterface;
use PHPUnit\Framework\TestCase;

final class BasicTest extends TestCase
{
    /** @var IndexedValueInterface */
    private $index;

    protected function setUp(): void
    {
        parent::setUp();
        $this->index = IndexedValue::make(5, 10, IndexType::EURO());
    }

    public function testBasic(): void
    {
        $this->assertEquals(10, $this->index->getValue());
        $this->assertEquals(5, $this->index->getOriginalValue());
        $this->assertEquals(IndexType::EURO(), $this->index->getType());
    }

    public function testRepository(): void
    {
        $repository = new BasicIndexedValueRepository;
        $repository->register('test', function() {
            return $this->index;
        });
        $repository->register('othertest', $this->index);

        $this->assertEquals($this->index, $repository->get('test'));
        $this->assertEquals($this->index, $repository->get('othertest'));
    }
}
