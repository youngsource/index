<?php

namespace Laudis\Index\Testing;

use DateTime;
use Laudis\Index\ContextualIndexedValueRepository;
use Laudis\Index\Contracts\ContextualIndexedValueRepositoryInterface;
use Laudis\Index\Contracts\IndexedValueInterface;
use Laudis\Index\IndexedValue;
use Laudis\Index\IndexType;
use PHPUnit\Framework\TestCase;

class ContextualIndexedValueRepositoryTest extends TestCase
{
    /** @var ContextualIndexedValueRepositoryInterface */
    private $repo;
    /** @var IndexedValueInterface */
    private $index1, $index2, $index3;

    public function testGetFromDate(): void
    {
        $index = $this->repo->getFromDate(DateTime::createFromFormat('Y-m-d', '2001-01-01'), 'index');
        self::assertSame($this->index1, $index);
        $index = $this->repo->getFromDate(DateTime::createFromFormat('Y-m-d', '1951-01-01'), 'index');
        self::assertSame($this->index2, $index);
        $index = $this->repo->getFromDate(DateTime::createFromFormat('Y-m-d', '2151-01-01'), 'index');
        self::assertSame($this->index3, $index);
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->repo = new ContextualIndexedValueRepository;
        $this->repo->addIndex('index', DateTime::createFromFormat('Y-m-d', '2000-01-01'),
            $this->index1 = $this->makeIndex());
        $this->repo->addIndex('index', DateTime::createFromFormat('Y-m-d', '1950-01-01'),
            $this->index2 = $this->makeIndex());
        $this->repo->addIndex('index', DateTime::createFromFormat('Y-m-d', '2150-01-01'),
            $this->index3 = $this->makeIndex());
    }

    private function makeIndex(): IndexedValueInterface
    {
        return new IndexedValue(0,0, IndexType::EURO());
    }
}
